import React from 'react';
import ReactDOM from 'react-dom';

//regular react components
const App = () => {
  return (
    <div>
      <h1>My App</h1>
      <p>This is some info</p>
    </div>
  );
}

/*
higher order component which accept another react component
and return a new component with additional feature
*/
const requireAuthentication = (WrappedComponent) => {
  return (props) => (
    <div>
      {props.isAuthenticated && <WrappedComponent />}
    </div>
  )
}

//the new component is stored in a variable and then render to the dom
const AuthInfo = requireAuthentication(App)

ReactDOM.render(<AuthInfo isAuthenticated={true} />, document.getElementById('app'));