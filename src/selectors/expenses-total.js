export default (expenses) => {
  if (expenses.length > 0) {
    return expenses.map((item) => {
      return item.amount;
    }).reduce((total, amount) => {
      return total + amount;
    });
  } else {
    return 0;
  }
}