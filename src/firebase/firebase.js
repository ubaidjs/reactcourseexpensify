import firebase from 'firebase/app';
import "firebase/database";
import "firebase/auth"

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDCfCmrKFgd4C-usJ-1zyylS_EIB3ppjOY",
  authDomain: "expensify-9cc7e.firebaseapp.com",
  databaseURL: "https://expensify-9cc7e.firebaseio.com",
  projectId: "expensify-9cc7e",
  storageBucket: "expensify-9cc7e.appspot.com",
  messagingSenderId: "654319293500"
};

firebase.initializeApp(config);

const database = firebase.database();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };
// database.ref('expenses').push({
//   description: 'Rent',
//   amount: 1234,
//   note: 'Paid rent for this month',
//   createdAt: 123456789
// });

// database.ref('age').set(21);
// database.ref('hobbies/0').set('something');
// database.ref('attrubutes').set({
//   height: 1.5,
//   weight: 45
// });


// subscribe to change in database and return new values
// database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val());
// });

// database.ref('age').set(22);