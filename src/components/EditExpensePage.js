import React from 'react';
import { connect } from 'react-redux';
import ExpenseForm from './ExpenseForm';
import { startRemoveExpense, startEditExpense } from '../actions/expenses';

const EditExpensePage = (props) => {
  return (
    <div>
      <ExpenseForm
        expense={props.expense}
        onSubmit={(expense) => {
          props.dispatch(startEditExpense(props.match.params.id, expense));
          props.history.push('/');
        }}
      />
      <button onClick={() => {
        props.dispatch(startRemoveExpense(props.expense.id)); // id is destructred from expense objest because removExpense expect an id.
        props.history.push('/');
      }}>remove</button>
    </div>
  );
};

const mapStateToProp = (state, props) => {
  return {
    expense: state.expenses.find((item) => {
      return item.id === props.match.params.id;
    })
  }
}

export default connect(mapStateToProp)(EditExpensePage);
