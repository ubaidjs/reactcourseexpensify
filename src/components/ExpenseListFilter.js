import React from 'react';
import { connect } from 'react-redux';
import 'react-dates/initialize';
import { DateRangePicker} from 'react-dates';
import { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate } from '../actions/filters';

class ExpenseListFilter extends React.Component {
  constructor(props) {
    super(props);
    this.onDatesChange = this.onDatesChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.state = {
      calendarFocused: null,
    }
  }

  onDatesChange({ startDate, endDate}) {
    this.props.dispatch(setStartDate(startDate));
    this.props.dispatch(setEndDate(endDate));
  }

  onFocusChange(calendarFocused) {
    this.setState(() => ({calendarFocused}))
  }

  render() {
    return (
      <div>
        <input type="text" placeholder="search..." value={this.props.filters.text} onChange={(e) => {
          this.props.dispatch(setTextFilter(e.target.value))
        }}/>
        <select value={this.props.filters.sortBy} onChange={(e) => {
          if (e.target.value === 'date') {
            this.props.dispatch(sortByDate());
          } else if (e.target.value === 'amount') {
            this.props.dispatch(sortByAmount());
          }
        }}>
          <option value="date">Date</option>
          <option value="amount">Amount</option>
        </select>
        <DateRangePicker 
          startDate={this.props.filters.startDate}
          startDateId="123"
          endDate={this.props.filters.endDate}
          endDateId="321"
          onDatesChange={this.onDatesChange}
          focusedInput={this.state.calendarFocused}
          onFocusChange={this.onFocusChange}
          numberOfMonths={1}
          isOutsideRange={() => false}
          showClearDates={true}
        />
      </div>
    )
  }
}

const mapStateToProp = (state) => {
  return {
    filters: state.filters
  }
}

export default connect(mapStateToProp)(ExpenseListFilter);