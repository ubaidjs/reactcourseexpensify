import React from 'react'
import { Link } from 'react-router-dom';
import moment from 'moment';

const ExpenseListItem = (props) => (
  <div>
    <Link to={`/edit/${props.details.id}`}><h3>{props.details.description}</h3></Link>
    <p>Amount: &#8377; {props.details.amount}</p>
    <p>Created At: {moment(props.details.createdAt).format('Do MMM, YYYY')}</p>
    <p>Note: {props.details.note}</p>
  </div>
);

export default ExpenseListItem;