import React from 'react';
import { connect } from 'react-redux';
import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../selectors/expenses';
import expensesTotal from '../selectors/expenses-total';

const ExpenseList = (props) => (
  <div>
    <h1>Expense List</h1>
    <p>Number of item: {props.expenses.length}</p>
    <h5>Total: &#8377; {props.total}</h5>
    {
      props.expenses.map((item) => (
        <ExpenseListItem key={item.id} details={item} />
      ))
    }
  </div>
);

const mapStateToProp = (state) => {
  return {
    expenses: selectExpenses(state.expenses, state.filters),
    total: expensesTotal(state.expenses)
  }
}

export default connect(mapStateToProp)(ExpenseList);