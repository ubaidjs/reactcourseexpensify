import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { startSetExpense } from './actions/expenses';
import { firebase } from './firebase/firebase';
import 'normalize.css/normalize.css';
import './styles/style.css';

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);
ReactDOM.render(<p>Loading...</p>, document.getElementById('app'));

store.dispatch(startSetExpense()).then(() => {
  ReactDOM.render(jsx, document.getElementById('app'));
})

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    console.log('logged in')
  } else {
    console.log('logged out')
  }
})