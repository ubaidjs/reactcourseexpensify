const add = (a, b) => {
  return a + b;
}

test('should add two number', () => {
  const result = add(4, 5);

  if ( result !== 9 ) {
    throw new Error(`you added 4 and 3. the result was ${result}. expected to be 9`);
  }
})
